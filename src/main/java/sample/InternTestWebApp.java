package sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InternTestWebApp {

    public static void main(String[] args) {
        SpringApplication.run(InternTestWebApp.class, args);
    }

}
