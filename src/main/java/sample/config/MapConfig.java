package sample.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class MapConfig {

    @Value("#{'${names}'.split(',')}")
    List<String> internNameList;

    @Value("#{'${sids}'.split(',')}")
    List<String> internSidList;

    @Bean
    Map<String, String> internMap(){
        Map<String, String> internMap = new HashMap<String, String>();

        for(int i = 0; i < internNameList.size(); i++){
            internMap.put(internSidList.get(i), internNameList.get(i));
        }

        return internMap;
    }
}
