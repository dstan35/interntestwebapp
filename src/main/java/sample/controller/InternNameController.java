package sample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class InternNameController {

    @Autowired
    Map<String, String> internMap;

    @GetMapping(path = "/getMap/")
    public Map<String, String> getMap(){
        return internMap;
    }

    @GetMapping(path = "/getName/{sid}")
    public ResponseEntity<String> getInternName(@PathVariable String sid){
        return new ResponseEntity<String>(internMap.get(sid), HttpStatus.OK);
    }

}

